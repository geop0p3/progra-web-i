<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Agencia Espacial X - Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i|Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	</head>
	<body>

		<section class="cover-5 text-center">
			<div class="container">
				<div class="row contact-details">
					<div class="col-sm-8 m-auto text-center">
						<h2>LOG IN</h2>
						<div class="divider"></div>
						<h4>Ingresa tu informacion</h4>
						<form action="check.php" method="post" class="contact-form mt-4">
							<div class="row">
								<div class="col-md-12">
                  <label for="contra">Usuario</label>
									<input type="text" class="form-control-custom mb-4" name="usuario" id="usuario">
								</div>
								<br/>
							</div>
							<div class="row">
								<div class="col-md-12">
                  <label for="contra">Contraseña</label>
									<input type="password" class="form-control-custom mb-4" rows="3" name="contra" id="contra"></input><br/>
									<button type="submit" class="btn btn-primary btn-lg mb-4">Enviar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>

	</body>
</html>