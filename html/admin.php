<?php
    session_start();
    include 'connect.php';
    $conectado = false;
    if (isset($_SESSION["conectado"])) {
        $conectado = $_SESSION{"conectado"};
    }

    if (!$conectado || $_SESSION["admin"] == 0) {
      if ($_SESSION["admin"] == 0) {
        header("Location: home.php");
      } else {
        header("Location: index.php");
      }
    }
        $sql = 'SELECT * FROM menu order by orderid';
        $result = $conn -> query ($sql);
        $total = $result -> num_rows;
     ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Area</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">

    <style media="screen">
    html,body{
      height:100%;
    }

      .alcien {
        height: 100%;
      }

      .barralateral {
        background: #263238 !important;
        color: #fafafa;
        padding: 15px;
        min-height: 100% !important;
      }
      .nav-link {
        color: #B39DDB !important;
        font-size: 22px;
      }
      ul li a:hover {
        color: grey !important;
      }

      .contenido {
        padding: 15px;
      }

      .rowcontenido {
        padding-top: 2%;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid alcien" style="height=100%">
      <div class="row alcien" style="height=100%; overflow=auto">
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 barralateral alcien">
          <?php
          echo '<h4>Bienvenido '.$_SESSION["usuario"].' </h4>';
           ?>
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link" href="home.php">Ver Sitio Web </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Usuarios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="secciones.php">Contenido</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="misc.php">Misc</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Cerrar Sesión</a>
            </li>
          </ul>
        </div>
        <div class="rowcontenido col-xs-6 col-sm-9 col-md-9 col-lg-10 alcien">
          <div class="card contenido">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
         <h2>Gestion de Usuarios</h2>
         <table class="table table-bordered">
           <thead>
             <tr>
              <th>id</th>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>Contraseña</th>
              <th>Correo Electrónico</th>
              <th>Admin?</th>
              <th>Guardar Usuario</th>
              <th>Borrar Usuario</th>
            <tr>
             <?php
             $acentos = $conn->query("SET NAMES 'utf8'");
               $sql = "SELECT * FROM usuarios";
               $resultado = $conn -> query ($sql);
             if ($resultado -> num_rows > 0) {
               while ($row = $resultado -> fetch_assoc()) {
                echo'
                <div class="row">
                <tr>
                <form action="actualizaruser.php" method="post">
                  <div class="form-group">
                  <td>'.$row["id"].'</td>
                  <input type="hidden" name="id" id="id" value="'.$row["id"].'">
                  <td><input type="text" name="nombre" id="nombre" value="'.$row["nombre"].'"></td>
                  <td><input type="text" name="usuario" id="usuario" value="'.$row["usuario"].'"></td>
                  <td><input type="password" name="pass" id="pass" value="'.$row["pass"].'"></td>
                  <td><input type="email" name="correo" id="correo" value="'.$row["correo"].'"></td>';

                  if ($row["admin"] == 1) {
                    echo'
                    <td><input type="checkbox" name="admin" id="admin" value="'.$row["admin"].'" checked></td>';
                  } else {
                    echo '<td><input type="checkbox" name="admin" id="admin" value="'.$row["admin"].'"></td>';
                  }
                  echo'
                  </div>
                  <td><button type="submit" class="btn btn-success">Actualizar</button></td>
                  <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#'.$row["id"].'">Borrar</button></td>
                </div>
                </form>
                </tr>
                </div>

                <div class="modal" id="'.$row["id"].'" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Atención</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <h2>Estas seguro que quieres borrar este usuario?</h2>
                      </div>
                      <div class="modal-footer">
                        <a href="borrar.php?id='.$row["id"].'" class="btn btn-danger" role="button">Borrar</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>';
               }
             }
             ?>
         </table>
                </div>
              </div>
            <div class="row">
              <div class="col-md-offset-10 col-md-2">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregarusuario">Agregar Usuario</button>
              </div>
            </div>

<div id="agregarusuario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Usuario</h4>
      </div>
      <div class="modal-body">
        <form id="agregar" action="crear.php" method="post">
        <div class="form-group">
          <label for="nombre2">Nombre:</label>
          <input type="text" class="form-control" id="nombre2" name="nombre2" required>
        </div>
       <div class="form-group">
         <label for="correo2">Correo Electrónico:</label>
         <input type="text" class="form-control" id="correo2" name="correo2" required>
       </div>
       <div class="form-group">
         <label for="usuario2">Usuario:</label>
         <input type="text" class="form-control" id="usuario2" name="usuario2" required>
       </div>
       <div class="form-group">
         <label for="pass2">Contraseña:</label>
         <input type="password" class="form-control" id="pass2" name="pass2" required>
       </div>
       <div class="checkbox">
         <label><input type="checkbox" id="admin2" name="admin2">Admin</label>
       </div>
       <div class="form-group row">
          <button type="submit" class="btn btn-success col-md-12">Guardar Nuevo</button>
         </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
  </body>
</html>

<?php
if($_GET){
if(isset($_GET['id']) && (isset($_GET['borrar']))){
  $id=$_GET['id'];
    borrar($id);
}elseif(isset($_GET['guardar'])&& (isset($_GET['id']))){
    $id=$_GET['id'];
    guardar($id);
}
}
    function borrar ($id){
      echo $id;
      $sql = 'DELETE FROM usuarios WHERE id ='.$id;
      $conn->query($sql);
      echo "testisx";
    }
    function guardar ($id){
      $sql = 'UPDATE usuarios SET nombre='.$nombre.'WHERE id ='.$id;
    }
?>
