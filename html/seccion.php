<?php
header("Content-Type: text/html;charset=utf-8");
        session_start();
            include 'connect.php';
            $conectado = false;
            if (isset($_SESSION["conectado"])) {
                $conectado = $_SESSION{"conectado"};
            }

            if (!$conectado) {
                header("Location: index.php");
            }


?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Agencia Espacial X</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i|Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	</head>
	<body>
			<?php include 'menu dark.php';
            ?>
            <?php
            $acentos = $conn->query("SET NAMES 'utf8'");
            if ($_GET["id"]) {
                $seccion=$_GET["id"];
            } else {
                header("Location: home.php");
            }
            if ($seccion) {
              $sql = 'SELECT * FROM contenido WHERE seccion='.$seccion.'';
              $result = $conn -> query($sql);
              if ($result -> num_rows > 0) {
                  while ($row = $result -> fetch_assoc()) {
                      if ($row["tipo"]==1) {
                          echo '<h2>'.$row["valor"]."</h2>";
                          echo '<div class="divider"></div>';
                      }
                      if ($row["tipo"]==2) {
                          echo '
  									<div class="mx-auto" style="padding-bottom:50px">
  										<h3>'.$row["valor"].'</h3>
  									</div>';
                      }
					  if ($row["tipo"]==3) {
                          echo '<div class="row"><div class="mx-auto">
            								<img class="mb-12 img-fluid" src="'.$row["extra"].'">
                            <div class="mx-auto col-md-6" style="padding:20px">
                            <p class="mx-auto">'.$row["valor"].'</p>
                            </div></div></div>';
                      }

					  if ($row["tipo"]==4  ) {
						  echo '
						 <div class="row justify-center">
							<div class="col-md-6 text-center">
								<img class="mb-4 img-fluid" src="'.$row["extra"].'">
							</div>
							<div class="col-md-5 text-center text-md-left">
								<h2 class="mb-4 mt-4">'.$row["valor"].'</h2>
								<p class="mb-4">'.$row["extra1"].'</p>
							</div>
						</div>
						  ';
                      }

                      if ($row["tipo"]==5) {
                                    echo '<div class="row justify-center">
                       							<div class="col-md-4 text-center text-md-left">
                       								<h2 class="mb-4 mt-4">'.$row["valor"].'</h2>
                       								<p class="mb-4">'.$row["extra1"].'</p>
                       							</div>
                                    <div class="col-md-6 text-center">
                       								<img class="mb-4 img-fluid" src="'.$row["extra"].'">
                       							</div>
                       						</div>';
                                }


                                if ($row["tipo"]==6) {
                                    echo '
          						  			<div class="container">
          						  				<div class="row justify-center">
                                    					<div class="col-md-6 text-center style="padding-bottom:50px"">
                                      					<h2 class="mb-12 mt-12">'.$row["valor"].'</h2>
          											</div>
          										</div>
          									</div>';
                                }

                      if ($row["tipo"]==7 && $row["extra1"]) {
                                    echo '
                                    <div class="row">
                              <div class="ml-4 col">
                              <h3>'.$row["valor"].'</h3>
                              <form class="contact-form mt-4">
                                <div class="row">
                                  <div class="col-md-12">
                                    <input type="text" class="form-control-custom mb-4" placeholder="Nombre2" value="Tu Nombre">
                                  </div>
                                  <div class="col-md-12">
                                    <input type="text" class="form-control-custom mb-4" placeholder="Correo 2" value="Correo Electrónico">
                                  </div>
                                  <br/>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <textarea class="form-control-custom mb-4" rows="3">Tu Mensaje</textarea><br/>
                                    <button type="submit" class="btn btn-primary btn-lg mb-4">Enviar Mensaje</button>
                                  </div>
                                </div>
                              </form>
                            </div><div class="col-md-5" style="padding-top:5%">
                            <h2 class="mx-auto">'.$row["extra1"].'</h2>
                            <h4 class="mx-auto">'.$row["extra"].'</h4>
                            </div></div>';
                          } else if ($row["tipo"]==7 && !$row["extra1"]) {
                            echo '
                            <div class="row">
                      <div class="col-md-4 mx-auto">
                      <h3>'.$row["valor"].'</h3>
                      <form class="contact-form mt-4">
                        <div class="row">
                          <div class="col-md-12">
                            <input type="text" class="form-control-custom mb-4" placeholder="Nombre2" value="Tu Nombre">
                          </div>
                          <div class="col-md-12">
                            <input type="text" class="form-control-custom mb-4" placeholder="Correo 2" value="Correo Electrónico">
                          </div>
                          <br/>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <textarea class="form-control-custom mb-4" rows="3">Tu Mensaje</textarea><br/>
                            <button type="submit" class="btn btn-primary btn-lg mb-4">Enviar Mensaje</button>
                          </div>
                        </div>
                      </form>
                    </div></div>';
                          }


          					   if ($row["tipo"]==8) {
                                    echo '
                                    <div class="row"><div class="col"><iframe
                                    width="100%"
                                    height="300"
                                    frameborder="0"
                                    scrolling="no"
                                    marginheight="0"
                                    marginwidth="0"
                                    src="https://maps.google.com/maps?q='.$row["valor"].'&hl=es;z=14&amp;output=embed">
                                    </iframe></div></div>
          								';
                                }
                                if ($row["tipo"]==9) {
                                              echo '<div class="col-md-6">
                      									<img class="mb-4 img-fluid" src="'.$row["extra"].'">
                                        <div class="col-md-12">
                                        <p class="mb-4">'.$row["valor"].'</p>
                                        </div></div>';
                                          }
                  }
              }
            }
            ?>
		<?php include 'footer.php';
        ?>

		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>

	</body>
</html>
