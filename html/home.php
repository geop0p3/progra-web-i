<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Agencia Espacial X</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i|Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	</head>
	<body>

		<?php
        session_start();
        include 'connect.php';
        $conectado = false;
        if (isset($_SESSION["conectado"])) {
            $conectado = $_SESSION{"conectado"};
        }

        if (!$conectado) {
            header("Location: index.php");
        }
         ?>
		<section class="cover-1 text-center">
			<?php include 'menu.php';
            ?>
			<div class="cover-container pb-5">
				<div class="cover-inner container">
					<h1 class="jumbotron-heading">Bienvenidos a la
						<strong>NUEVA ERA</strong>
					</h1>
					<p class="lead">La Agencia Espacial X, es la primera de México en lanzar un cohete a la luna.</p>
					<p>
						<a href="seccion.php?id=3" class="btn btn-primary btn-lg mb-2 mr-2 ml-2">Ver Portafolio</a>
						<a href="seccion.php?id=4" class="btn btn-outline-white btn-lg mb-2 ml-2 ml-2">Contacto</a>
					</p>
				</div>
			</div>
		</section>

		<section class="content-1">
			<div class="container">
				<div class="row justify-center">
					<div class="col-md-6 text-center">
						<img class="mb-4 img-fluid" src="images/es_7000_1.png">
					</div>
					<div class="col-md-6 text-center text-md-left">
						<h2 class="mb-4 mt-4">Cohetes Espaciales</h2>
						<p class="mb-4">Utilizando los mejores materiales, como fibras de nano carbono y de aliaciones de metales como alumino, nos aseguramos de fabricar los mejores cohetes. Además de utilizar sistemas de aviación totalmente avanzados</p>
						<p>
							<a class="btn btn-outline-secondary" href="seccion.php?id=3" role="button">Ver Portafolio
							</a>
						</p>
					</div>
				</div>
			</div>
		</section>

		<section class="content-2">
			<div class="container">
				<div class="row justify-center">
					<div class="col-md-6 text-center text-md-left pl-5">
						<h2 class="mb-4 mt-4">Satelites</h2>
						<p class="mb-4">Lanzamos todo tipo de satelites, incluyendo aquellos que funcionan con ondas de radio o super frecuencias.</p>
						<p>
							<a class="btn btn-outline-secondary" href="seccion.php?id=3" role="button">Ver Portafolio
							</a>
						</p>
					</div>
					<div class="col-md-6 text-center">
						<img class="mb-4 img-fluid" src="images/satelite.png">
					</div>
				</div>
			</div>
		</section>

		<section class="content-4">
			<div class="container">
				<div class="row justify-center mt-5">
					<div class="col-md-6 pl-5 pr-5 text-center">
						<img class="mb-4 img-fluid" src="images/CanSat-Strawberry.png">
					</div>
					<div class="col-md-6 text-left">
						<h2>Can Sats y Avionicos</h2>
						<p class="lead mt-4 mb-5">
							Nuestros productos también abarcan el sector amateur, con nuestros kits de cansats podrás lanzar pequeños satelites a la atmosfera para recaudar datos.
						</p>

						<div class="row">
							<div class="col-md-6 col-feature mb-4">
								<h4 class="mb-3">Can Sats</h4>
								<p>Estos kits vienen equipados con sensores de altitud, temperatura, radiación, localización gps y un pequeño booster.</p>
							</div>
							<div class="col-md-6 col-feature mb-4">
								<h4 class="mb-3">Avionicos</h4>
								<p>Nuestros micro-controladores. te permiten crear sistemas de control y computadoras de vuelo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php include 'footer.php';
        ?>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>

	</body>
</html>
