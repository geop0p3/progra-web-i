<?php
    session_start();
    include 'connect.php';
    $conectado = false;
    if (isset($_SESSION["conectado"])) {
        $conectado = $_SESSION{"conectado"};
    }

    if (!$conectado || $_SESSION["admin"] == 0) {
      if ($_SESSION["admin"] == 0) {
        header("Location: home.php");
      } else {
        header("Location: index.php");
      }
    }
        $sql = 'SELECT * FROM menu order by orderid';
        $result = $conn -> query ($sql);
        $total = $result -> num_rows;
     ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Area</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">

    <style media="screen">
    html,body{
      height:100%;
    }

      .alcien {
        height: 100%;
      }

      .barralateral {
        background: #263238 !important;
        color: #fafafa;
        padding: 15px;
        min-height: 100% !important;
        overflow: auto;
      }
      .nav-link {
        color: #B39DDB !important;
        font-size: 22px;
      }
      ul li a:hover {
        color: grey !important;
      }

      .contenido {
        padding: 15px;
      }

      .rowcontenido {
        padding-top: 2%;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid alcien" style="height=100%">
      <div class="row alcien" style="height=100%; overflow=auto">
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 barralateral alcien">
          <?php
          echo '<h4>Bienvenido '.$_SESSION["usuario"].' </h4>';
           ?>
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="home.php">Ver Sitio Web </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="admin.php">Usuarios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contenido</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="misc.php">Misc</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Cerrar Sesión</a>
            </li>
          </ul>
        </div>
        <div class="rowcontenido col-xs-6 col-sm-9 col-md-9 col-lg-10">
          <div class="card contenido">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
         <h2>Gestion de Contenido</h2>
         <table class="table table-bordered">
           <thead>
             <tr>
              <th>Id</th>
              <th>Tipo de Contenido</th>
              <th>Contenido</th>
              <th>Sección</th>
              <th>Imagen de Contenido</th>
              <th>Párrafo extra</th>
              <th>Guardar</th>
              <th>Borrar</th>
            <tr>
             <?php
             $acentos = $conn->query("SET NAMES 'utf8'");
               $sql = "SELECT * FROM contenido
INNER JOIN nombre ON contenido.tipo = nombre.Id
WHERE tipo < 6 Order by seccion asc, tipo asc";
               $resultado = $conn -> query ($sql);
             if ($resultado -> num_rows > 0 ) {
               while ($row = $resultado -> fetch_assoc()) {
                echo'

                <tr>
                <form action="actualizarseccion.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                  <td>'.$row["id"].'</td>
                  <td><input type="hidden" name="id" id="id" value="'.$row["id"].'">
                  <select class="form-control" name="tipo" id="tipo">
                    <option value="'.$row["tipo"].'">'.$row["Nombre"].'</option>
                    <option value="1">Encabezado</option>
                    <option value="2">Subtitulo</option>
                    <option value="3">Imagen y texto centrado</option>
                    <option value="4">Imagen y texto</option>
                    <option value="5">Texto e imagen</option>
                  </select></td>
                  <td><textarea class="form-control" name="valor" id="valor" rows="3" maxlength="350">'.$row["valor"].'</textarea></td>';
                  $sql2 = "SELECT * FROM menu";
                  $resultado2 = $conn -> query ($sql2);
                  if ($resultado2 -> num_rows > 0 ) {
                  while ($row2 = $resultado2 -> fetch_assoc()) {
                    if ($row["seccion"]==$row2["id"]) {
                      $nombreselect=$row2["texto"];
                    }
                  }
                }
                  echo'
                  <td><select class="form-control" name="seccion" id="seccion">
                  <option value="'.$row["seccion"].'">'.$nombreselect.'</option>
                  <option value="2">Acerca</option>
                  <option value="3">Portafolio</option>
                  <option value="4">Contacto</option>
                </select></td>
                <td>
                <label for="imagen">'.$row["extra"].'</label>
                <input type="file" class="form-control-file" name="imagen" id="imagen" accept="image/png, image/jpeg, image/jpg">
                </td>
                <td><textarea class="form-control" name="extra" id="extra" rows="3" maxlength="150">'.$row["extra1"].'</textarea></td>
                  </div>
                  <td><button type="submit" class="btn btn-success">Actualizar</button></td>
                  <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#'.$row["id"].'">Borrar</button></td>
                </div>
                </form>
                </tr>
                </div>

                <div class="modal" id="'.$row["id"].'" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Atención</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <h2>Estas seguro que quieres borrar este contenido?</h2>
                      </div>
                      <div class="modal-footer">
                        <a href="borrarseccion.php?id='.$row["id"].'" class="btn btn-danger" role="button">Borrar</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>';
               }
             }
             ?>
         </table>
                </div>
              </div>
            <div class="row">
              <div class="col-md-offset-10 col-md-2">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregarusuario">Agregar Contenido</button>
              </div>
            </div>

<div id="agregarusuario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Contenido</h4>
      </div>
      <div class="modal-body">
        <form id="agregar" action="crearseccion.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label class="control-label" for="tipo2">Tipo de Contenido</label>
        <select id="tipo2" name="tipo2" class="select form-control">
          <option value="1">Encabezado</option>
          <option value="2">Subtitulo</option>
          <option value="3">Imagen y texto centrado</option>
          <option value="4">Imagen y texto</option>
          <option value="5">Texto e imagen</option>
        </select>
      </div>
    <div class="form-group">
      <label class="control-label" for="valor2">Contenido</label>
        <textarea id="valor2" name="valor2" cols="40" rows="5" class="form-control" maxlength="350" required></textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="valor2">Parrafo Extra</label>
        <textarea id="extra2" name="extra2" cols="40" rows="5" class="form-control" maxlength="150"></textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="seccion2">Seccion</label>
        <select id="seccion2" name="seccion2" class="select form-control">
          <option value="2">Acerca</option>
          <option value="3">Portafolio</option>
          <option value="4">Contacto</option>
        </select>
    </div>
    <div class="form-group">
    <label for="imagen2">Imagen</label>
    <input type="file" class="form-control-file" name="imagen2" id="imagen2">
  </div>
    <div class="form-group row">
       <button type="submit" class="btn btn-success col-md-12">Guardar Nuevo</button>
      </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
  </body>
</html>

<?php
if($_GET){
if(isset($_GET['id']) && (isset($_GET['borrar']))){
  $id=$_GET['id'];
    borrar($id);
}elseif(isset($_GET['guardar'])&& (isset($_GET['id']))){
    $id=$_GET['id'];
    guardar($id);
}
}
    function borrar ($id){
      echo $id;
      $sql = 'DELETE FROM usuarios WHERE id ='.$id;
      $conn->query($sql);
      echo "testisx";
    }
    function guardar ($id){
      $sql = 'UPDATE usuarios SET nombre='.$nombre.'WHERE id ='.$id;
    }
?>
