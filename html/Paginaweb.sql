-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-07-2018 a las 00:53:42
-- Versión del servidor: 5.7.22-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Paginaweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE `contenido` (
  `id` int(11) NOT NULL,
  `valor` varchar(2000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  `extra` varchar(2500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `extra1` varchar(2000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`id`, `valor`, `tipo`, `seccion`, `extra`, `extra1`) VALUES
(1, 'Nosotros', 1, 2, '', ''),
(2, 'Portafolio', 1, 3, '', ''),
(3, 'Contactanos', 1, 4, '', ''),
(5, 'Estamos para servirle, no dudes en contactarnos y contestaremos lo más antes posible', 2, 4, '', ''),
(6, 'Cotizaciones Gratis', 2, 4, '', ''),
(7, 'Inicios de la empresa', 2, 2, '', ''),
(8, 'Somos una empresa 100% mexicana que busca posicionar al país dentro de los mejores.', 2, 2, '', ''),
(9, 'Contamos con una gama amplía de proyectos, trabajos que hemos estado haciendo por más de 8 años.\r\n', 2, 3, '', ''),
(10, 'Iniciada en el 2012, la agencia espacial X propuso la carrera espacial en México y en latino américa.\r\n							<br>\r\n							Todo comienza gracias a Carlos Flores, el cual busca establecer la presencia mexicana en la carrera espacial, dando así, el inicio de la Agencia X.', 3, 2, 'images/oficinas.jpg', ''),
(11, 'Bacon ipsum dolor amet shank prosciutto sausage capicola strip steak hamburger jowl shoulder tongue short ribs. Kielbasa pig pork swine strip steak porchetta filet mignon. Cow boudin pork belly, pork loin doner meatball strip steak andouille hamburger venison corned beef flank filet mignon. Shankle pastrami flank, porchetta prosciutto chuck cupim ribeye shank pancetta.', 3, 2, 'images/greyhound.jpg', ''),
(13, 'Cohetes Espaciales', 4, 3, 'images/es_7000_1.png', 'Utilizando los mejores materiales, como fibras de nano carbono y de aliaciones de metales como aluminio, nos aseguramos de fabricar los mejores cohetes. Además de utilizar sistemas de aviación totalmente avanzados'),
(45, 'Árbol', 7, 4, '', ''),
(49, '32.5209744,-116.9912951', 8, 4, '', ''),
(58, 'Cotiza con Nosotros', 7, 3, 'Bacon ipsum dolor amet pork loin rump shankle turkey, burgdoggen kielbasa pork belly', 'Los mejores precios y el mejor producto a tu alcance');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `url` varchar(20) NOT NULL,
  `texto` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `orderid`, `url`, `texto`) VALUES
(1, 1, 'home.php', 'Home'),
(2, 2, 'nada', 'Acerca'),
(3, 3, 'nada', 'Portafolio'),
(4, 4, 'nada', 'Contacto'),
(5, 5, 'logout.php', 'Salir'),
(6, 6, 'admin', 'Usuarios'),
(7, 7, 'admin', 'Secciones'),
(8, 8, 'admin', 'Subsecciones'),
(9, 9, 'admin.php', 'Admin Area');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nombre`
--

CREATE TABLE `nombre` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nombre`
--

INSERT INTO `nombre` (`Id`, `Nombre`) VALUES
(1, 'Encabezado'),
(2, 'Subtitulo'),
(3, 'Imagen y texto centrado'),
(4, 'Imagen y texto'),
(5, 'Texto e imagen'),
(7, 'Forma'),
(8, 'Mapa'),
(9, 'Imagen');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` char(20) NOT NULL,
  `correo` char(30) NOT NULL,
  `pass` char(30) NOT NULL,
  `usuario` char(10) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `correo`, `pass`, `usuario`, `admin`) VALUES
(2, 'El targas', 'johntargas@gmail.com', 'targas123', 'targas3', 1),
(5, 'Carlos', 'geop0p3@gmail.com', 'popeye123', 'geop0p3', 1),
(6, 'Tomas Delgado', 'Tomas@correo.com', 'tomas', 'tomas', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nombre`
--
ALTER TABLE `nombre`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
